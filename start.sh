#!/bin/bash

rm -rf MonitoringClient
echo $(uname -n) > hostname
echo $(ip route get 8.8.8.8 | awk '{print $NF; exit}') > ip

git clone https://github.com/dipfrost/MonitoringClient.git
chmod +x MonitoringClient/docker
chmod +x MonitoringClient/py/sh/*
cd MonitoringClient
docker build -t goclient .

docker-compose up